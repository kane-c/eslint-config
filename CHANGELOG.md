# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.2.0] - 2023-08-10

### Changed

- Update readme for Prettier 3.
- Fix TypeScript member ordering rule.
- Use strictest TypeScript rules.

## [4.1.0] - 2023-08-07

### Changed

- Bump dependencies
- Update Prettier config for version 3

## [4.0.0] - 2023-05-15

### Changed

- Update sample scripts in the readme to lint markdown files.
- Enable caching in ESLint.
- Bump Prettier to 2.8.8. This may result in formatting changes to your code.
- Bump `eslint-plugin-deprecation`.

## [3.0.1] - 2023-03-28

### Changed

- Apply Jest and Testing Library rules to test files only.
- Bump Prettier to 2.8.7. This may result in formatting changes to your code.
- Fix `eslint-plugin-deprecation` under TypeScript 5.

## [3.0.0] - 2022-09-17

### Added

- Add plugins for Jest and Testing Library.
- Add support for `.cts` and `.mts` files.

### Changed

- Bump Prettier to 2.7.1. This may result in formatting changes to your code. The npm script `lint:prettier` now uses Prettier's `--cache` option.

## [2.2.0] - 2022-06-03

### Added

- Lint Markdown files in the example setup.
- Improve `react/require-default-props` rule for use with TypeScript. This may result in formatting changes to your code.
- Move back from `@delagen/eslint-plugin-deprecation` to `eslint-plugin-deprecation`.

## [2.1.0] - 2022-05-14

### Added

- A shared Prettier config. Check the updated README for usage.

## [2.0.0] - 2022-05-13

### Breaking

- Add the missing `airbnb` ESLint preset for TypeScript files.
- Add `react/jsx-runtime` rules. This config now assumes using the new React JSX runtime.
- Bump eslint-config-airbnb-typescript to v17. This may result in formatting changes to your code.
- Bump Prettier to 2.6.2. This may result in formatting changes to your code.

## [1.0.1] - 2022-03-30

### Added

- This CHANGELOG file.

### Changed

- Update the README to reflect new versioning.
- Bump Prettier to 2.6.1. This may result in formatting changes to your code.

## [1.0.0] - 2022-03-30

Initial release
