# @kane-c's ESLint config

[![pipeline status](https://gitlab.com/kane-c/eslint-config/badges/main/pipeline.svg)](https://gitlab.com/kane-c/eslint-config/-/commits/main)

- Designed for use with ES6, React, TypeScript, Node and browser JS.
- Extends Airbnb and typescript-eslint's rules
- Adds browser compatibility checking
- Adds deprecation checks
- Adds comment linting
- Adds Prettier
- Enforces a maximum line length of 79 ala PEP8
- Enforces sorted imports, object keys, JSX attributes, and TypeScript type keys

## Assumptions

- You use React, with the new JSX runtime
- You use TypeScript or Babel

## Setup

Add the repo to your project:

```bash
npm install https://gitlab.com/kane-c/eslint-config/-/archive/4.2.0/eslint-config-4.2.0.tar.gz
```

Use this tarball URL so you don't need git to install it.

Add ESLint config to your `package.json`:

```json
{
  "eslintConfig": {
    "extends": "@kane-c"
  }
}
```

Add Prettier to your `package.json`:

```json
{
  "prettier": "@kane-c/eslint-config/prettier-config"
}
```

Add some extra scripts to the `scripts` section of your `package.json`:

```json
{
  "scripts": {
    "format": "prettier --write './**/*.{js,jsx,cjs,mjs,ts,tsx,cts,mts,json,scss,yaml,yml}'",
    "lint": "npm run -s lint:json && npm run -s lint:js && npm run -s lint:ts && npm run -s lint:css && npm run -s lint:scss && npm run -s lint:md && npm run -s lint:yaml",
    "lint:css": "npm run -s lint:prettier -- './**/*.css'",
    "lint:eslint": "eslint --cache --cache-strategy content --ext=js,jsx,cjs,mjs,ts,tsx,cts,mts --ignore-path .gitignore --max-warnings 0",
    "lint:js": "npm run -s lint:prettier -- './**/*.{js,jsx,cjs,mjs,ts,tsx,cts,mts,json}' && npm run -s lint:eslint -- .",
    "lint:json": "npm run -s lint:prettier -- './**/*.json'",
    "lint:md": "npm run -s lint:prettier -- './**/*.md'",
    "lint:prettier": "prettier --cache --ignore-path .gitignore --check",
    "lint:scss": "npm run -s lint:prettier -- './**/*.scss'",
    "lint:ts": "tsc",
    "lint:ts:watch": "npm run -s lint:ts -- --watch",
    "lint:yaml": "npm run -s lint:prettier -- './**/*.{yaml,yml}'"
  }
}
```

### Bonus: Lint on commit with `husky` and `lint-staged`:

Install some extra dev dependencies.

```bash
npm install --save-dev husky lint-staged
npm set-script pre-commit "lint-staged && npm run -s lint:ts"
npm set-script prepare "husky install"
npm run prepare
husky add .husky/pre-commit "npm run -s pre-commit"
```

And add this to your `package.json`.

```json
{
  "lint-staged": {
    "*.json": "npm run -s lint:prettier",
    "*.@(js|jsx|cjs|mjs|ts|tsx|cts|mts)": [
      "npm run -s lint:prettier",
      "npm run -s lint:eslint"
    ],
    "*.@(css|scss)": "npm run -s lint:prettier",
    "*.md": "npm run -s lint:prettier",
    "*.@(yaml|yml)": "npm run -s lint:prettier"
  }
}
```
