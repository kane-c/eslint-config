const { defaultOrder } = require('@typescript-eslint/eslint-plugin/dist/rules/member-ordering');

// Keep this formatted JSON style so it is easy to copy to a `package.json`
const rules = {
  "max-len": ["warn", 79],
  "sort-keys": [
    "warn",
    "asc",
    {
      "natural": true
    }
  ],
  "eslint-comments/disable-enable-pair": [
    "error",
    {
      "allowWholeFile": true
    }
  ],
  "import/extensions": [
    "error",
    "ignorePackages",
    {
      "js": "never",
      "mjs": "never",
      "jsx": "never",
      "ts": "never",
      "tsx": "never"
    }
  ],
  "import/order": [
    "error",
    {
      "alphabetize": {
        "order": "asc"
      },
      "groups": [
        "builtin",
        "external",
        "internal",
        "parent",
        "sibling",
        "index",
        "object",
        "type"
      ],
      "newlines-between": "always"
    }
  ],
  "jsx-a11y/anchor-is-valid": [
    "warn",
    {
      "aspects": ["invalidHref", "preferButton"],
      "components": ["Link"],
      "specialLink": ["hrefLeft", "hrefRight"]
    }
  ],
  "react/jsx-sort-props": "warn",
  "react/sort-prop-types": "warn",
  // TODO Remove once Airbnb allows this
  "react/static-property-placement": ["warn", "static public field"],
  "react/require-default-props": [
    "error",
    {
      "functions": "defaultArguments"
    }
  ],
  "sort-destructure-keys/sort-destructure-keys": "warn",
  "sort-imports": [
    "warn",
    {
      "ignoreDeclarationSort": true
    }
  ]
};
const test = {
  "extends": [
    "plugin:jest/all",
    "plugin:jest-dom/recommended",
    "plugin:jest-formatting/strict",
    "plugin:testing-library/react"
  ],
  "plugins": ["jest", "jest-formatting", "testing-library"]
};
const extensions = ["js", "jsx", "cjs", "mjs"];
module.exports = {
  "overrides": [
    {
      "files": extensions.map((e) => `*.${e}`),
      "extends": [
        "airbnb",
        "airbnb/hooks",
        "prettier",
        "plugin:compat/recommended",
        "plugin:eslint-comments/recommended",
        "plugin:react/jsx-runtime"
      ],
      "parser": "@babel/eslint-parser",
      "plugins": ["sort-destructure-keys"],
      "overrides": [
        {
          "files": extensions.map((e) => `*.test.${e}`),
          ...test
        }
      ],
      "rules": rules
    },
    {
      "files": extensions.map((e) => `*.${e.replace("j", "t")}`),
      "extends": [
        "airbnb",
        "airbnb-typescript",
        "airbnb/hooks",
        "plugin:compat/recommended",
        "plugin:eslint-comments/recommended",
        "plugin:react/jsx-runtime",
        "plugin:@typescript-eslint/strict-type-checked",
        "plugin:@typescript-eslint/stylistic-type-checked",
        "prettier"
      ],
      "parserOptions": {
        "project": "./tsconfig.json"
      },
      "plugins": ["deprecation", "sort-destructure-keys"],
      "overrides": [
        {
          "files": extensions.map((e) => `*.test.${e.replace("j", "t")}`),
          ...test
        }
      ],
      "rules": {
        "@typescript-eslint/member-ordering": [
          "warn",
          {
            "default": {
              "memberTypes": defaultOrder,
              "order": "alphabetically"
            }
          }
        ],
        "deprecation/deprecation": "warn",
        ...rules
      }
    }
  ],
  "rules": rules,
  "settings": {
    "import/resolver": {
      "node": {
        "moduleDirectory": ["node_modules", "."]
      }
    }
  }
};
